#!/bin/bash
set -e

wd=$(dirname "$0")
cp "$wd/vivaldi-custom.css" /opt/vivaldi/resources/vivaldi/style/custom.css

echo 'copied custom css'

if grep -q 'import "custom.css"' /opt/vivaldi/resources/vivaldi/style/common.css; then
  echo 'custom css already imported'
else
  sed -i '1s/^/@import "custom.css";/' /opt/vivaldi/resources/vivaldi/style/common.css
fi
