# hi

it's a dotfile repo!

## installation

Execute `install`. This automatically detects your OS, but the `-c` flag allows you to specify an alternative config file by full name (e.g. `ubuntu.conf.yaml`). Pass `-s` to install a few useful SystemD units for Arch.

## common/greatest hits

- [espanso](https://gitlab.com/dmfay/dotfiles/-/tree/master/espanso)
- neovim
  - [vimrc](https://gitlab.com/dmfay/dotfiles/-/blob/master/vimrc)
  - [ddu](https://gitlab.com/dmfay/dotfiles/-/blob/master/vim/plugins/ddu.vim) universal thing-selector
- [psql](https://gitlab.com/dmfay/dotfiles/-/blob/master/psqlrc) and [theme](https://gitlab.com/dmfay/dotfiles/-/blob/master/pspg_theme_warm_dark)
- [rofi](https://gitlab.com/dmfay/dotfiles/-/tree/master/rofi)
- [wezterm](https://gitlab.com/dmfay/dotfiles/-/tree/master/wezterm)
- zsh
  - [zshrc](https://gitlab.com/dmfay/dotfiles/-/blob/master/zshrc)
  - [other config](https://gitlab.com/dmfay/dotfiles/-/tree/master/zsh)
  - [vim mode with indicator](https://gitlab.com/dmfay/dotfiles/-/blob/master/zsh/mode.zsh)
  - build and display [digraphs](https://gitlab.com/dmfay/dotfiles/-/blob/master/zsh/digraph.zsh)
  - maintain a library of useful [sql scripts](https://gitlab.com/dmfay/dotfiles/-/blob/master/zsh/sql.zsh)
  - render PostGIS [geographies](https://gitlab.com/dmfay/dotfiles/-/blob/master/zsh/pgisd.zsh) and [rasters](https://gitlab.com/dmfay/dotfiles/-/blob/master/zsh/pgrast.zsh)

## arch

- [eww](https://gitlab.com/dmfay/dotfiles/-/tree/master/eww) desktop widgets
- [sway](https://gitlab.com/dmfay/dotfiles/-/tree/master/sway) window manager

## ubuntu

- [i3](https://gitlab.com/dmfay/dotfiles/-/tree/master/i3) window manager
- [polybar](https://gitlab.com/dmfay/dotfiles/-/tree/master/polybar) bar

## osx

nothing special
