source ~/.dotfiles/zsh/zcomet/zcomet.zsh

zcomet load "mafredri/zsh-async"
zcomet load "hlissner/zsh-autopair"
zcomet load "softmoth/zsh-vim-mode"
zcomet load "lainiwa/zsh-manydots-magic" # fork of knu/zsh-manydots-magic with plugin
zcomet load "zsh-users/zsh-syntax-highlighting"

zcomet compinit
