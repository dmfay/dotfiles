#!/bin/zsh

function pgrast() {
  if [ "$#" -lt 7 ]; then
    echo "Syntax: pgrast dbname tblname colname xmin ymin xmax ymax [reclass]"
    echo ""
    echo "
Renders PostGIS raster tiles from tblname.colname within the specified bounds
to stdout as PNG images. Rasters must be unionable, that is, same-aligned (same
scale, skew, SRID, and at least one corner shared).

The optional reclass parameter is a PostGIS reclassarg which can mask or merge
pixel values within the first band. For example:

    0-12]:0, [13-14]:1, (15-255:0

combines values 13 and 14, while eliminating every other value (in an origin
raster with pixeltype 8BUI or 8-bit unsigned int).

See https://postgis.net/docs/reclassarg.html for details.
    "
    return 0
  fi

  local tblname=$2
  local colname=$3
  local xmin=$4
  local ymin=$5
  local xmax=$6
  local ymax=$7
  local reclass=$8

  local rastarg=$colname

  if [ -n "$8" ]; then
    rastarg="st_reclass($colname, '$reclass', st_bandpixeltype($colname))"
  fi

  local query="
    set postgis.gdal_enabled_drivers to 'PNG';
    select
      encode(
        st_aspng(
          rast => st_colormap(
            rast => $rastarg,
            nband => 1,
            colormap => 'pseudocolor'
            /*
            colormap => '100% 156 107 128 255
                        81.1% 156 107 128 255
                          80% 110 145 110 255
                        60.1% 110 145 110 255
                          60% 156 137 105 255
                        40.1% 156 137 105 255
                          40% 101 130 199 255
                        20.1% 101 130 199 255
                          20% 162 118 182 255
                         0.1% 162 118 182 255
                           0% 102 152 170 255
                           nv   0   0   0   0'
                           */
          )
        ),
        'hex'
      ) as rast
    from (
      select st_union($colname) as rast
      from $tblname
      where st_upperleftx($colname) between $xmin and $xmax
        and st_upperlefty($colname) between $ymin and $ymax
    )
  "

  local raw=$(psql $1 --no-psqlrc --tuples-only -c "$query" | tail -2)
  local result=("${(s/|/)raw}")
  local img=$result[1]

  if [ -t 1 ]; then
    echo $img | xxd -p -r | wezterm imgcat
  else
    # in a pipe, no imgcat
    echo $img | xxd -p -r
  fi
}
