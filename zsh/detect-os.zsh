if [[ `uname` == 'Linux' ]]; then
  export OS=`lsb_release -i -s | tr '[:upper:]' '[:lower:]'`
elif [[ `uname` == 'Darwin' ]]; then
  export OS=osx
fi
