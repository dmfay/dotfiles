#!/bin/zsh

function pgplot() {
  local xdata=""

  while getopts t opt; do
    case "$opt" in
      t)
        xdata="set xdata time;"
        ;;
    esac
  done

  if test "$xdata"; then
    # cleaning up args in the while loop gets unpredictable
    shift
  fi

  if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Syntax: pgplot dbname 'select x, y from tbl' [plotting_style]"
    echo "
plotting_style is any 2d style, e.g. lines, impulses; default is points."
    return 0
  fi

  local raw=$(psql $1 --no-psqlrc -c "$2")
  local result=("${(s/|/)raw}")
  local plottingstyle=${3:-points}

  local source="
  set terminal pngcairo;
  set timefmt '%Y-%m-%d';
  $xdata
  plot '<cat' using 1:2 title columnheader with $plottingstyle;
  "

  if [ -t 1 ]; then
    # in terminal, fancy output
    echo "$result" | gnuplot -e "$source" | wezterm imgcat
  else
    # in a pipe, dump to stdout
    echo "$result" | gnuplot -e "$source"
  fi
}
