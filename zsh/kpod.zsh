function kpod() {
  kubectl get pods | grep -i "^$1" | grep -v "Terminating" | head -1 | cut -d ' ' -f 1
}
