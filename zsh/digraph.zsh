# digraph: dot shortcuts from stdin
#
# syntax:
#   $ digraph <<EOF
#   (dot here)
#   EOF
#
# OR:
#   $ digraph
#   (dot here)
#   <Ctrl-D>
#
# requires:
#   graphviz
#   wezterm

function digraph() {
  local DIGRAPH_TEMPLATE='
    digraph {
      graph [
        bgcolor="transparent"
      ];

      node [
        color="#9c6b80"
        fontcolor="#f5eff3"
        fontname="Inconsolata LGC"
        shape="rect"
        style="rounded,filled"
      ];

      edge [
        color="#9ccbdd"
        fontname="Inconsolata LGC"
        fontcolor="#f5eff3"
        labelfontcolor="#f5eff3"
        dir="forward"
        arrowhead="normal"
      ];

      concentrate=true;

      %s
    }
  '

  if [ -t 1 ]; then
    # we're sending output to the terminal
    printf "$DIGRAPH_TEMPLATE" "$(cat /dev/stdin)" | dot -Tpng | wezterm imgcat
  else
    # we're in a pipe
    printf "$DIGRAPH_TEMPLATE" "$(cat /dev/stdin)" | dot -Tpng
  fi
}
