#!/bin/zsh

zmodload zsh/mathfunc

function pgisd() {
  # if srid=unknown, this is a geometry plot
  local srid="0"
  local xmin=${4:-0}
  local ymin=${5:-0}
  local xmax=${6:-100}
  local ymax=${7:-100}
  local width=$(( xmax - xmin ))
  local height=$(( ymax - ymin ))
  local xmid=$(( xmax - width / 2.0 ))
  local ymid=$(( ymax - height / 2.0 ))
  local scale=$(( width / 500.0 ))
  local emptyraster="
        st_makeemptyraster(
          width => $width,
          height => $height,
          upperleftx => $xmin,
          upperlefty => $ymin,
          pixelsize => $scale
        )
  "

  while getopts s: opt; do
    case "$opt" in
      s)
        srid="$OPTARG";
        xmin=${4:-"-180"}
        ymin=${5:-"-90"}
        xmax=${6:-180}
        ymax=${7:-90}
        width=$(( int(ceil(xmax - xmin)) ))
        height=$(( int(ceil(ymax - ymin)) ))
        xmid=$(( xmax - width / 2.0 ))
        ymid=$(( ymax - height / 2.0 ))
        scale=$(( width / 900.0 ))
        emptyraster="
              st_makeemptyraster(
                width => $width,
                height => $height,
                upperleftx => $xmin,
                upperlefty => $ymin,
                scalex => $scale,
                scaley => -$scale,
                skewx => 0.0,
                skewy => 0.0,
                srid => $srid
              )
        "
    esac
  done

  if [ "$srid" -ne 0 ]; then
    # cleaning up args in the while loop gets unpredictable
    shift
  fi

  if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Syntax: pgisd [-sSRID] dbname path/to/script.sql [xmin ymin xmax ymax]"
    echo ""
    echo "
Runs a script file and plots geometry columns into PNG images. If a SRID is
given, the image will include a cross dividing the north-south and east-west
halves.

The script must select at least one column prefixed \`geom\`, which is rendered
as a PNG and piped to standard output. It may also select columns prefixed
\`ewkt\` and \`text\`, which are st_collected and converted to the Extended
Well-Known Text format, and string_aggregated respectively. \`geom\`, \`ewkt\`,
and \`text\` columns must be specified in that order!

Multiple \`geom\` columns may be specified and plotted in-shell, but if piped
to a file only the first is rendered.

Bounds without a SRID default to (0, 0) - (100, 100), and with a SRID to the
WGS84 lat-lon (-90, -180) - (90, 180). They can be specified as the last four
arguments. Scale is automatically adjusted to what seems like a vaguely
sensible degree.
"
    return 0
  fi

  local script=$(<$2)

  # dry run to determine geometry columns
  local headerstr=$(psql $1 --no-psqlrc -c "$script" | head -1 | tr -d ' ')
  local headers=("${(s/|/)headerstr}")
  local geom_columns=()
  local ewkt_columns=()
  local text_columns=()

  # build our output select list
  local select="count(*)"

  for header in $headers; do
    if [[ $header == geom* ]]; then
      geom_columns+=$header

      select="
      $select,
      encode(
        st_aspng(
          rast => st_colormap(
            rast => st_union(
              st_asraster(
                geom => case
                  when '$srid' > 0 then st_collect(ranked.$header::geometry, grid.geom)
                  else ranked.$header::geometry
                end,
                ref => ref.rast,
                pixeltype => '8BUI',
                value => rownum
              )
            ),
            nband => 1,
            -- colormap => 'pseudocolor'
            colormap => '100% 156 107 128 255
                        81.1% 156 107 128 255
                          80% 110 145 110 255
                        60.1% 110 145 110 255
                          60% 156 137 105 255
                        40.1% 156 137 105 255
                          40% 101 130 199 255
                        20.1% 101 130 199 255
                          20% 162 118 182 255
                         0.1% 162 118 182 255
                           0% 102 152 170 255
                           nv   0   0   0   0'
            /*
            colormap => '100% 156 107 128 255
                           0% 156 107 128 255
                           nv   0   0   0   0'
                           */
          )
        ),
        'hex'
      )"
    elif [[ $header == ewkt* ]]; then
      ewkt_columns+=$header
      select="
      $select,
      st_asewkt(st_collect($header), 2)
      "
    elif [[ $header == text* ]]; then
      text_columns+=$header
      select="
      $select,
      string_agg($header::text, E'; ')
      "
    fi
  done

  local query="
    set postgis.gdal_enabled_drivers to 'PNG';
    with ref as (
      select $emptyraster as rast
    ), grid as (
      select st_collect(
        st_setsrid(st_makeline(st_makepoint($xmid, $ymin), st_makepoint($xmid, $ymax)), $srid),
        st_setsrid(st_makeline(st_makepoint($xmin, $ymid), st_makepoint($xmax, $ymid)), $srid)
      ) as geom
    ), script as (
      ${script%;}
    ), ranked as (
      -- distinguish returned records by row number since they won't necessarily
      -- have a consistent & correctly typed identifier
      select row_number() over () as rownum, *
      from script
    )
    select $select
    from ranked
    cross join ref
    cross join grid
  "

  local raw=$(psql $1 --no-psqlrc --tuples-only -c "$query" | tail -2)
  local result=("${(s/|/)raw}")

  if [ -t 1 ]; then
    # in terminal, fancy output
    local i=1
    echo "$result[$i] records"

    # render all images
    for col in $geom_columns; do
      i=$(($i+1))

      local img=$result[$i]

      echo $col
      echo $img | xxd -p -r | wezterm imgcat
    done

    # print all ewkt
    for col in $ewkt_columns; do
      i=$(($i+1))

      echo "$col: $result[$i]"
      echo ""
    done

    # print all text
    for col in $text_columns; do
      i=$(($i+1))

      echo "$col: $result[$i]"
      echo ""
    done
  else
    # in a pipe, render just the first image (column 2 after the count)
    local img=$result[2]
    echo $img | xxd -p -r
  fi
}
