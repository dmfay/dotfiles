# Run scripts in ~/sql/$dbname against a database. Database names are compared
# up to the first underscore, so scripts in ~/sql/dbname may be run against
# dbname_test or dbname_backup_yyyymmdd.
#
# Based on https://www.arp242.net/zshrc.html#run-stored-sql-queries
#
# Syntax:
#   sql dbname db_script.sql
#   sql dbname_test db_script.sql
#   sql dbname 'select * from a_table'

function sql() {
  local PAGER="pspg --ignore-case --no-sound --quit-if-one-screen --only-for-tables -X -s 22"
  local CMD="psql -X -P linestyle=unicode $1"
  local DB="$(__base_dbname $1)"
  local F="$HOME/sql/$DB/$2"

  shift

  if [[ -f "$F" ]]; then
    eval "psql $DB -f $F"
  else
    eval "$CMD" <<< "$@" | eval "$PAGER"
  fi
}

function __base_dbname() {
  echo ${1//_*/''}
}

function _sql() {
  local curcontext="$curcontext" state line
  typeset -A opt_args

  _arguments -C \
    "1:database:->db" \
    "2:file:_files -W ~/sql/$(__base_dbname $words[2])/"

  case "$state" in
    db)
      _alternative 'databases:database:($(psql --no-psqlrc --tuples-only -c "select datname from pg_database"))'
      ;;
  esac
}

compdef _sql sql
