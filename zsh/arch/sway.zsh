# startup command

alias starts="XDG_RUNTIME_DIR=/run/user/1000 sway"

if [ -n WAYLAND_DISPLAY ]; then
  # set XCURSOR_THEME for wezterm
  export XCURSOR_THEME=Bibata-Original-Ice
fi
