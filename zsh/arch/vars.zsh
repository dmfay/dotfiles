# ssh-agent is started by a systemd user service, link up with the socket
export SSH_AUTH_SOCK=/run/user/1000/ssh-agent.socket

export JAVA_HOME=/usr/lib/jvm/default
export GROOVY_HOME=/usr/share/groovy
