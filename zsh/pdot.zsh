# pdot
#
# requires:
#   graphviz
#   wezterm

function pdot() {
  if [[ "$#" = 0 || "$@" = "--help" || "$@" = "-h" ]]; then
    command pdot "$@"
    return 0
  fi

  local DIGRAPH_TEMPLATE='
    digraph {
      graph [
        bgcolor="#3c3445"
        peripheries=0
      ];

      node [
        color="#9c6b80"
        fontcolor="#f5eff3"
        fontname="Inconsolata LGC"
        shape="rect"
        style="rounded,filled"
      ];

      edge [
        color="#9ccbdd"
        fontcolor="#f5eff3"
        fontname="Inconsolata LGC"
        dir="forward"
        arrowhead="normal"
      ];

      concentrate=true;

      %s
    }
  '

  local DOT_DEFN=$(command pdot "$@")

  if [ -t 1 ]; then
    # we're sending output to the terminal
    printf "$DIGRAPH_TEMPLATE" "$DOT_DEFN" | dot -Tpng | wezterm imgcat
  else
    # we're in a pipe
    printf "$DIGRAPH_TEMPLATE" "$DOT_DEFN" | dot -Tpng
  fi
}

# tab-completions
function _pdot() {
  local curcontext="$curcontext" state line
  local dbquery="select datname from pg_database"
  local ops="fks views refs triggers grants"
  local tblquery="select case when table_schema <> 'public' then table_schema || '.' || table_name else table_name end from information_schema.tables where table_schema not in ('pg_catalog', 'information_schema') and table_type = 'BASE TABLE' order by 1"
  local viewquery="select case when table_schema <> 'public' then table_schema || '.' || table_name else table_name end from information_schema.tables where table_schema not in ('pg_catalog', 'information_schema') union select case when table_schema <> 'public' then table_schema || '.' || table_name else table_name end from information_schema.views where table_schema not in ('pg_catalog', 'information_schema') order by 1"
  local fnquery="select case when routine_schema <> 'public' then routine_schema || '.' || routine_name else routine_name end from information_schema.routines where routine_schema not in ('pg_catalog', 'information_schema') order by 1"
  local usrquery="select rolname from pg_roles"
  typeset -A opt_args

  _arguments -C \
    "1:database:->db" \
    "2:op:->op" \
    "3:arg:->arg"

  case "$state" in
    db)
      _alternative "databases:database:($(psql --no-psqlrc --tuples-only -c $dbquery))"
      ;;
    op)
      _alternative "ops:op:($ops)"
      ;;
    arg)
      case $words[3] in
        (fks|triggers)
          _alternative "tables:table:($(psql $words[2] --no-psqlrc --tuples-only -c $tblquery 2>/dev/null))"
          ;;
        (views)
          _alternative "views:view:($(psql $words[2] --no-psqlrc --tuples-only -c $viewquery 2>/dev/null))"
          ;;
        (refs)
          _alternative "functions:function:($(psql $words[2] --no-psqlrc --tuples-only -c $fnquery 2>/dev/null))"
          ;;
        (grants)
          _alternative "roles:role:($(psql $words[2] --no-psqlrc --tuples-only -c $usrquery 2>/dev/null))"
          ;;
      esac
      ;;
  esac
}

compdef _pdot pdot
