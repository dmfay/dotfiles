"""""""""""""""""""
" plugin management

set nocompatible
filetype off

set runtimepath^=~/.vim/dein.vim

call dein#begin(expand('~/.cache/dein'))

call dein#add('Shougo/dein.vim')                " plugin management

" ddu
call dein#add('vim-denops/denops.vim')
call dein#add('Shougo/ddu.vim')
call dein#add('Shougo/ddu-ui-ff')
call dein#add('Shougo/ddu-kind-file')
call dein#add('Shougo/ddu-kind-word')
call dein#add('Shougo/ddu-source-line')
call dein#add('Shougo/ddu-source-register')
call dein#add('shun/ddu-source-rg')
call dein#add('shun/ddu-source-buffer')
call dein#add('flow6852/ddu-source-qf')
call dein#add('matsui54/ddu-source-file_external')
call dein#add('matsui54/ddu-source-command_history')
call dein#add('k-ota106/ddu-source-marks')
call dein#add('Shougo/ddu-filter-converter_display_word')
call dein#add('Shougo/ddu-filter-matcher_substring')
call dein#add('yuki-yano/ddu-filter-fzf')

" integrations, major modules
call dein#add('simnalamburt/vim-mundo')         " better undo
call dein#add('Shougo/vimproc.vim', {'build': 'make'})  " async exec
call dein#add('editorconfig/editorconfig-vim')  " respect .editorconfig

if has('nvim')
  call dein#add('nvim-lua/plenary.nvim')        " lua plugin framework for null-ls
  call dein#add('glacambre/firenvim', { 'hook_post_update': { _ -> firenvim#install(0) } }) " browser integration!
  call dein#add('rlane/pounce.nvim')            " modal, hinted, fuzzy motion
  call dein#add('nvim-treesitter/nvim-treesitter') " syntax trees!
  call dein#add('nvim-treesitter/playground')   " inspect syntax trees!
  call dein#add('roobert/node-type.nvim')       " node type on statusline!
  call dein#add('williamboman/mason.nvim')      " language servers....
  call dein#add('williamboman/mason-lspconfig.nvim') " ....automatically....
  call dein#add('neovim/nvim-lspconfig')        " ....configured....
  call dein#add('nvimtools/none-ls.nvim')       " ....plus linters/formatters....
  call dein#add('onsails/diaglist.nvim')        " ....in quickfix
  call dein#add('jbyuki/venn.nvim')             " diagramming
else
  call dein#add('ludovicchabant/vim-gutentags') " automatic tag file management
  call dein#add('neomake/neomake')              " run static analysis checks
endif

" editor interface tweaks
call dein#add('kshenoy/vim-signature')          " marks in the sign column
call dein#add('simeji/winresizer')              " modal resize with C-e
call dein#add('unblevable/quick-scope')         " highlight f/F/t/T

if has('nvim')
  call dein#add('nvim-treesitter/nvim-treesitter-context') " context subwindow for long chunks
  call dein#add('lukas-reineke/indent-blankline.nvim') " show indent guides
  call dein#add('NvChad/nvim-colorizer.lua')    " colors
  call dein#add('lewis6991/gitsigns.nvim')      " git line symbols
  call dein#add('karb94/neoscroll.nvim')
  call dein#add('levouh/tint.nvim')             " tint inactive windows
  call dein#add('abecodes/tabout.nvim')         " tab through closing punctuation
else
  call dein#add('wellle/context.vim')             " show indent-based context
  call dein#add('rrethy/vim-hexokinase', { 'build': 'make hexokinase' }) " colors
  call dein#add('airblade/vim-gitgutter')       " git line symbols
endif

" editing improvements
call dein#add('zirrostig/vim-schlepp')          " move visual selections around
call dein#add('cohama/lexima.vim')              " auto-close symbol pairs
call dein#add('mg979/vim-visual-multi')         " multicursor mode
call dein#add('tommcdo/vim-exchange')           " exchange text objects
call dein#add('tpope/vim-commentary')           " comment toggles
call dein#add('tpope/vim-fugitive')             " git commands
call dein#add('tpope/vim-repeat')               " repeat plugin-map commands
call dein#add('tpope/vim-surround')             " work with brackets/tags/etc
call dein#add('tpope/vim-unimpaired')           " miscellaneous improvements
call dein#add('tpope/vim-speeddating')          " C-A/C-X increment/decrement dates
call dein#add('machakann/vim-swap')             " reorder items
call dein#add('dominikduda/vim_current_word')   " highlight word under cursor
call dein#add('andymass/vim-matchup')           " better matching with %
call dein#add('ojroques/vim-oscyank')           " yank across ssh

if has ('nvim')
  call dein#add('nacro90/numb.nvim')            " :num navigation peeking
  call dein#add('lambdalisue/suda.vim')         " work around for !sudo tee trick
  call dein#add('Eandrju/cellular-automaton.nvim') " fun!
endif

" syntax
call dein#add('sheerun/vim-polyglot')           " base syntax package
call dein#add('othree/xml.vim')                 " override xml plugin
call dein#add('tmhedberg/SimpylFold')           " python code folding
call dein#add('kergoth/vim-bitbake')            " bitbake filetype & syntax
call dein#add('pulkomandy/c.vim')               " better c
call dein#add('https://gitlab.com/itaranto/id3.nvim') " mp3 id3 tags

" markdown editing
call dein#add('godlygeek/tabular')              " tables
call dein#add('plasticboy/vim-markdown')        " override markdown plugin
call dein#add('mzlogin/vim-markdown-toc')       " table of contents

" textobjs
call dein#add('kana/vim-textobj-user')          " base
call dein#add('jceb/vim-textobj-uri')           " u
call dein#add('Chun-Yang/vim-textobj-chunk')    " c
call dein#add('reedes/vim-textobj-sentence')    " s and parens, improved
call dein#add('tpope/vim-jdaddy')               " j
call dein#add('wellle/targets.vim')             " seeking, separators, a, b, q

call dein#end()

filetype plugin indent on

if dein#check_install()
  call dein#install()
endif
