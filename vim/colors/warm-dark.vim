hi clear
syntax reset

let g:colors_name = 'warm-dark'

" passthrough seems to be working better now and explicitly setting
" terminal_color_n results in it displaying darker colors for some
" reason, so disabling this

" let g:terminal_color_0='#604c60'
" let g:terminal_color_8='#604c60'
" " red
" let g:terminal_color_1='#9c6b80'
" let g:terminal_color_9='#dc98ac'
" " green
" let g:terminal_color_2='#6e916e'
" let g:terminal_color_10='#96c497'
" " yellow
" let g:terminal_color_3='#9c8969'
" let g:terminal_color_11='#d9bd8f'
" " blue
" let g:terminal_color_4='#6582c7'
" let g:terminal_color_12='#98b2fb'
" " magenta
" let g:terminal_color_5='#a276b6'
" let g:terminal_color_13='#d6ade8'
" " cyan
" let g:terminal_color_6='#6698aa'
" let g:terminal_color_14='#9ccbdd'
" " white
" let g:terminal_color_7='#cfc7cd'
" let g:terminal_color_15='#f5eff3'

" editor

hi ColorColumn gui=NONE guifg=NONE guibg=#3c3445
hi Conceal gui=NONE guifg=#9c6b80 guibg=NONE
hi Cursor gui=reverse guifg=NONE guibg=NONE
hi CursorColumn gui=NONE guifg=NONE guibg=NONE
hi CursorLine gui=NONE guifg=NONE guibg=#604c60
hi CursorLineNr gui=NONE guifg=NONE guibg=#604c60
hi DiffAdd gui=standout guifg=#6e916e guibg=NONE
hi DiffChange gui=bold guifg=NONE guibg=NONE
hi DiffDelete gui=standout guifg=#9c6b80 guibg=NONE
hi DiffText gui=standout guifg=NONE guibg=NONE
hi Directory gui=bold guifg=#f5eff3 guibg=NONE
hi FoldColumn gui=NONE guifg=#9c6b80 guibg=NONE
hi Folded gui=NONE guifg=#dc98ac guibg=NONE
hi IncSearch gui=NONE guifg=#3c3445 guibg=#9c6b80
hi LineNr gui=NONE guifg=NONE guibg=NONE
hi Pmenu gui=NONE guifg=NONE guibg=#3c3445
hi PmenuSbar gui=NONE guifg=NONE guibg=#9c6b80
hi PmenuSel gui=NONE guifg=NONE guibg=#9c6b80
hi PmenuThumb gui=NONE guifg=NONE guibg=#dc98ac
hi Question gui=NONE guifg=NONE guibg=NONE
hi Search gui=italic guifg=#3c3445 guibg=#9ccbdd
hi SignColumn gui=NONE guifg=#d9bd8f guibg=NONE
hi StatusLine gui=NONE guifg=NONE guibg=NONE
hi StatusLineNC gui=italic guifg=#dc98ac guibg=NONE
hi TabLine gui=NONE guifg=#3c3445 guibg=#dc98ac
hi TabLineFill gui=NONE guifg=NONE guibg=#604c60
hi TabLineSel gui=reverse guifg=#3c3445 guibg=#dc98ac
hi Title gui=NONE guifg=NONE guibg=NONE
hi VertSplit gui=NONE guifg=NONE guibg=NONE
hi Visual gui=NONE guifg=NONE guibg=#604c60
hi VisualNOS gui=NONE guifg=NONE guibg=NONE
hi WildMenu gui=NONE guifg=NONE guibg=NONE
hi FloatBorder gui=NONE guifg=#9c6b80 guibg=NONE

" text

hi MatchParen gui=reverse guifg=NONE guibg=NONE
hi NonText gui=NONE guifg=#3c3445 guibg=NONE
hi Normal gui=NONE guifg=#f5eff3 guibg=NONE
hi NormalFloat gui=NONE guifg=NONE guibg=NONE
hi Strong gui=bold guifg=NONE guibg=NONE
hi Emphasis gui=italic guifg=NONE guibg=NONE
hi Strike gui=strikethrough guifg=NONE guibg=NONE
hi Url gui=underline guifg=#d9bd8f guibg=NONE

hi ErrorMsg gui=bold guifg=#9ccbdd guibg=NONE
hi WarningMsg gui=NONE guifg=#d9bd8f guibg=NONE
hi ModeMsg gui=NONE guifg=NONE guibg=NONE
hi MoreMsg gui=NONE guifg=NONE guibg=NONE
hi SpellBad gui=undercurl guisp=NONE guifg=NONE guibg=NONE
hi SpellCap gui=undercurl guisp=NONE guifg=NONE guibg=NONE
hi SpellLocal gui=undercurl guisp=NONE guifg=NONE guibg=NONE
hi SpellRare gui=undercurl guisp=NONE guifg=NONE guibg=NONE

" syntax

hi Comment gui=italic guifg=#dc98ac guibg=NONE

hi Constant gui=NONE guifg=#d9bd8f guibg=NONE
hi String gui=NONE guifg=#96c497 guibg=NONE
hi Character gui=NONE guifg=#96c497 guibg=NONE
hi Number gui=NONE guifg=#d6ade8 guibg=NONE
hi Boolean gui=NONE guifg=#d6ade8 guibg=NONE
hi Float gui=NONE guifg=#d6ade8 guibg=NONE

hi Identifier gui=NONE guifg=NONE guibg=NONE
hi Function gui=NONE guifg=NONE guibg=NONE

hi Statement gui=NONE guifg=#9ccbdd guibg=NONE
hi Conditional gui=NONE guifg=#9ccbdd guibg=NONE
hi Repeat gui=NONE guifg=#9ccbdd guibg=NONE
hi Label gui=NONE guifg=#d6ade8 guibg=NONE
hi Operator gui=NONE guifg=#dc98ac guibg=NONE
hi Keyword gui=NONE guifg=#9ccbdd guibg=NONE
hi Exception gui=NONE guifg=#9ccbdd guibg=NONE

hi PreProc gui=NONE guifg=#9ccbdd guibg=NONE
hi Include gui=NONE guifg=#9ccbdd guibg=NONE
hi Define gui=NONE guifg=#98b2fb guibg=NONE
hi Macro gui=NONE guifg=NONE guibg=NONE
hi PreCondit gui=NONE guifg=#98b2fb guibg=NONE

hi Type gui=NONE guifg=#d9bd8f guibg=NONE
hi StorageClass gui=NONE guifg=#d9bd8f guibg=NONE
hi Structure gui=NONE guifg=#d9bd8f guibg=NONE
hi Typedef gui=NONE guifg=#98b2fb guibg=NONE

hi Special gui=NONE guifg=#9ccbdd guibg=NONE
hi SpecialChar gui=NONE guifg=#dc98ac guibg=NONE
hi Tag gui=NONE guifg=#dc98ac guibg=NONE
hi Delimiter gui=NONE guifg=#dc98ac guibg=NONE
hi SpecialComment gui=NONE guifg=#98b2fb guibg=NONE

hi Debug gui=NONE guifg=#9ccbdd guibg=NONE
hi Underlined gui=underline guifg=NONE guibg=NONE
hi Ignore gui=NONE guifg=NONE guibg=NONE
hi Error gui=bold,underline guifg=#9ccbdd guibg=NONE
hi Warning gui=NONE guifg=#d9bd8f guibg=NONE " see also WarningMsg
hi Todo gui=bold guifg=#3c3445 guibg=#9ccbdd

if has('nvim')
  " tree-sitter captures

  hi link @comment Comment
  hi link @error ErrorMsg
  hi link @none None
  hi link @preproc PreProc
  hi link @define Define
  hi link @operator Operator

  hi link @punctuation.delimiter Delimiter
  hi link @punctuation.bracket SpecialChar
  hi link @punctuation.special SpecialChar

  hi link @string String
  hi link @string.regex Constant
  hi link @string.escape Constant
  hi link @string.special Special

  hi link @character Character
  hi link @character.special Character

  hi link @boolean Boolean
  hi link @number Number
  hi link @float Float

  hi link @function Function
  hi link @function.builtin Constant
  hi link @function.call Normal
  hi link @function.macro Macro

  hi link @method Function
  hi link @method.call Normal

  hi link @constructor Macro
  hi link @parameter Identifier

  hi link @keyword Keyword
  hi link @keyword.function Keyword
  hi link @keyword.operator Operator
  hi link @keyword.return Keyword

  hi link @conditional Conditional
  hi link @repeat Repeat
  hi link @debug Debug
  hi link @label Label
  hi link @include Include
  hi link @exception Exception

  hi link @type Type
  hi link @type.builtin Type
  hi link @type.definition TypeDef
  hi link @type.qualifier Type

  hi link @storageclass StorageClass
  hi link @attribute PreProc
  hi link @field Normal
  hi link @property Normal

  hi link @variable Normal
  hi link @variable.builtin Constant

  hi link @constant Constant
  hi link @constant.builtin Constant
  hi link @constant.macro Macro

  hi link @namespace Define
  hi link @symbol Special

  hi link @text Normal
  hi link @text.strong Strong
  hi link @text.emphasis Emphasis
  hi link @text.underline Underlined
  hi link @text.strike Strike
  hi link @text.title Title
  hi link @text.literal Normal
  hi link @text.uri Url " also captures inside of <a> tags....
  hi link @text.math Normal
  hi link @text.environment Normal
  hi link @text.environment.name Normal
  hi link @text.reference Normal
  hi link @text.todo Todo
  hi link @text.note Normal
  hi link @text.warning Warning
  hi link @text.danger Error

  hi link @text.diff.add DiffAdd
  hi link @text.diff.delete DiffDelete

  hi link @tag Tag
  hi link @tag.attribute Label
  hi link @tag.delimiter Tag

  hi link @conceal Conceal

  " nope, not happening
  " hi link @spell Todo

  hi link @variable.global Constant

  hi link @definition Normal
  hi link @definition.constant Constant
  hi link @definition.function Function
  hi link @definition.method Function
  hi link @definition.var Normal
  hi link @definition.parameter Identifier
  hi link @definition.macro Macro
  hi link @definition.type Type
  hi link @definition.field Todo
  hi link @definition.enum Structure
  hi link @definition.namespace Define
  hi link @definition.import Include
  hi link @definition.associated Todo

  hi link @scope Todo
  hi link @reference Todo
endif

" plugins

hi SpecialKey gui=NONE guifg=#616161 guibg=NONE
hi GitGutterAdd gui=NONE guifg=#96c497 guibg=NONE
hi GitGutterChange gui=NONE guifg=#d9bd8f guibg=NONE
hi GitGutterChangeDelete gui=NONE guifg=#d9bd8f guibg=NONE
hi GitGutterDelete gui=NONE guifg=#dc98ac guibg=NONE
hi SignatureMarkText gui=NONE guifg=#9ccbdd guibg=NONE
hi CSVColumnEven gui=NONE guifg=#dc98ac guibg=NONE
hi CSVColumnOdd gui=NONE guifg=#f5eff3 guibg=NONE
hi CSVColumnHeaderEven gui=NONE guifg=#dc98ac guibg=NONE
hi CSVColumnHeaderOdd gui=NONE guifg=#f5eff3 guibg=NONE
hi DiagnosticError gui=bold guifg=#9ccbdd guibg=NONE
hi DiagnosticWarn gui=bold guifg=#d9bd8f guibg=NONE
hi DiagnosticInfo gui=NONE guifg=#d6ade8 guibg=NONE
hi DiagnosticHint gui=NONE guifg=#dc98ac guibg=NONE
hi CurrentWordTwins gui=underline guifg=NONE guibg=NONE
hi QuickScopePrimary gui=bold guifg=#9ccbdd guibg=NONE
hi QuickScopeSecondary gui=NONE guifg=#98b2fb guibg=NONE
hi PounceMatch gui=NONE guifg=#3c3445 guibg=#98b2fb
hi PounceGap gui=NONE guifg=#3c3445 guibg=#604c60
hi PounceAccept gui=NONE guifg=#9c6b80 guibg=#9ccbdd
hi PounceAcceptBest gui=NONE guifg=#9c6b80 guibg=#f5eff3
hi GitSignsAdd gui=NONE guifg=#96c497 guibg=NONE
hi GitSignsChange gui=NONE guifg=#d9bd8f guibg=NONE
hi GitSignsChangeDelete gui=NONE guifg=#d9bd8f guibg=NONE
hi GitSignsDelete gui=NONE guifg=#dc98ac guibg=NONE

match Statement /\d\{1,3}\ze\%(\d\{6}\)*\d\{3}\>/
