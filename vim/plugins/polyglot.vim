let g:javascript_plugin_jsdoc = 1         " vim-javascript: enable jsdoc
let g:sql_type_default = 'pgsql'          " pgsql: always assume postgres
let g:vim_jsx_pretty_colorful_config = 1  " vim-jsx-pretty: full color
