if has('nvim')
lua <<EOF
  require("mason").setup {
    registries = {
      "github:mason-org/mason-registry",
      "lua:mason-registry.index"
    },
    ui = {
      border = "double",
      icons = {
        package_installed = "✓"
      },
    }
  }

  require("mason-lspconfig").setup {
    ensure_installed = {
      -- lsp servers
      "bashls",
      "cssls",
      "dockerls",
      "dotls",
      "eslint",
      "graphql",
      "html",
      "jdtls", -- java
      "jsonls",
      "julials",
      "marksman", -- markdown
      "pylsp", -- python
      "rust_analyzer",
      "lua_ls",
      "ts_ls", -- typescript
      "vimls",
      "yamlls",
      -- linters can no longer be ensure_installed?
      -- "sqlfluff",
      -- "pylint",
      -- "yamllint",
      -- "vint",
      -- "shellcheck"
    },
  }

  require("mason-lspconfig").setup_handlers({
    function (server_name) -- default handler (optional)
      require("lspconfig")[server_name].setup {}
    end,
  })

  require("null-ls").setup({
    sources = {
      -- require("null-ls").builtins.diagnostics.checkmake,
      require("null-ls").builtins.diagnostics.sqlfluff.with({
        extra_args = {
          "--dialect", "postgres",
        },
      }),
    },
  })

  local border = {
    {"╔", "FloatBorder"},
    {"═", "FloatBorder"},
    {"╗", "FloatBorder"},
    {"║", "FloatBorder"},
    {"╝", "FloatBorder"},
    {"═", "FloatBorder"},
    {"╚", "FloatBorder"},
    {"║", "FloatBorder"},
  }

  local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
  function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
    opts = opts or {}
    opts.border = opts.border or border
    opts.filetype = "lsp-hover"
    return orig_util_open_floating_preview(contents, syntax, opts, ...)
  end

  vim.o.updatetime = 250
  vim.cmd [[autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]
EOF
endif
