if !has('nvim')
  let g:context_nvim_no_redraw = 1 " https://github.com/wellle/context.vim/issues/68
  let g:context_border_char = '─'
endif
