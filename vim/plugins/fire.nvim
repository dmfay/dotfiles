if has('nvim')
lua <<EOF
  local config = {
    globalSettings = {
      ignoreKeys = {
        all = {
          '<C-->',
          '<C-=>',
          '<C-Esc>',
          '<C-Tab>',
          '<SC-Tab>',
          '<C-t>',
          '<S-C-t>',
          '<D-t>',
          '<S-D-t>',
          '<S-C-r>',
          '<S-D-r>',
        },
        normal = {
          '<C-T>',
          '<C-l>',
          '<C-1>',
          '<C-2>',
          '<C-3>',
          '<C-4>',
          '<C-5>',
          '<C-6>',
          '<C-7>',
          '<C-8>',
          '<C-9>',
          '<C-0>',
          '<D-t>',
          '<D-l>',
          '<D-1>',
          '<D-2>',
          '<D-3>',
          '<D-4>',
          '<D-5>',
          '<D-6>',
          '<D-7>',
          '<D-8>',
          '<D-9>',
          '<D-0>',
        },
        insert = {
          '<C-v>',
          '<D-v>',
        },
      },
    },
    localSettings = {
      ['.*'] = {
        takeover = 'once',
      },
    },
  }

  vim.g.firenvim_config = config
  vim.cmd("luafile " .. vim.fn.stdpath("config") .. "/plugins/firenvim.lua")
EOF

  if exists('g:started_by_firenvim')

    nnoremap <Tab> <Cmd>:call rpcnotify(firenvim#get_chan(), 'firenvim_focus_next')<CR>
    nnoremap <S-Tab> <Cmd>:call rpcnotify(firenvim#get_chan(), 'firenvim_focus_prev')<CR>

    function! AdjustMinimumLines(timer)
      if &lines < 15
        set lines=15
      endif
    endfunction

    function! OnUIEnter(event) abort
      if 'Firenvim' ==# get(get(nvim_get_chan_info(a:event.chan), 'client', {}), 'name', '')
        set laststatus=0

        au BufReadPost *.txt ++once call timer_start(1, {_ -> feedkeys("GA")})

        " have to set height async https://github.com/glacambre/firenvim/issues/800
        call timer_start(1, function("AdjustMinimumLines"))
      endif
    endfunction

    autocmd UIEnter * call OnUIEnter(deepcopy(v:event))

    augroup firenvim_types
      autocmd!
      autocmd BufEnter gitlab.com_*.txt set filetype=markdown
      autocmd BufEnter github.com_*.txt set filetype=markdown
    augroup end
  endif
endif
