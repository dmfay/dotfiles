if !has('nvim')
  let g:neomake_warning_sign = {'text': '?', 'texthl': 'WarningMsg'}
  let g:neomake_error_sign = {'text': '!', 'texthl': 'ErrorMsg'}

  if executable('eslint')
    let g:neomake_javascript_enabled_makers = ['eslint']
    let g:neomake_typescript_enabled_makers = ['eslint']
  endif

  if executable('sqlint')
    let g:neomake_sql_enabled_makers = ['sqlint']
  endif

  if executable('shellcheck')
    let g:neomake_sh_enabled_makers = ['shellcheck']
  endif

  if executable('pylint')
    let g:neomake_python_enabled_makers = ['pylint']
  endif

  if executable('yamllint')
    let g:neomake_yaml_enabled_makers = ['yamllint']
  endif

  augroup lint_on_write_or_switch
    autocmd!
    autocmd BufWritePost,BufEnter * Neomake
  augroup end
endif
