if !has('nvim')
  set signcolumn=yes
  let g:gitgutter_max_signs = 500
  let g:gitgutter_realtime = 1
  let g:gitgutter_sign_removed = 'x'
endif
