call ddu#custom#patch_global(#{
  \   ui: 'ff',
  \   uiParams: #{
  \     ff: #{
  \       replaceCol: match(getline('.')[: col('.') - 1], '\f*$') + 1,
  \       ignoreEmpty: v:false,
  \       autoResize: v:false,
  \       cursorPos: 0,
  \     }
  \   },
  \   kindOptions: #{
  \     file: #{
  \       defaultAction: 'open',
  \     },
  \     command_history: #{
  \       defaultAction: 'edit',
  \     },
  \   },
  \   filterParams: #{
  \     matcher_substring: #{
  \       highlightMatched: 'Search',
  \     },
  \     matcher_fzf: #{
  \       highlightMatched: 'Search',
  \     },
  \   },
  \   sources: [
  \     #{ name: 'rg', },
  \     #{ name: 'buffer', },
  \     #{ name: 'command_history', },
  \     #{ name: 'file_external', },
  \     #{ name: 'marks', },
  \     #{ name: 'register', },
  \   ],
  \   sourceOptions: #{
  \     _: #{
  \       matchers: [
  \         'converter_display_word',
  \         'matcher_fzf',
  \       ],
  \       sorters: ['sorter_fzf'],
  \       converters: [],
  \     },
  \     file_external: #{
  \       path: getcwd()
  \     },
  \     register: #{
  \       defaultAction: 'insert',
  \     },
  \   },
  \   sourceParams: #{
  \     buffer: #{
  \       orderby: 'desc'
  \     },
  \     file_external: #{
  \       cmd: ['fd', '.', '-t', 'f']
  \     },
  \     rg: #{
  \       args: ['--column', '--no-heading', '--smart-case', '--color', 'never'],
  \       highlights: #{path: 'Url'},
  \     },
  \     qf: #{
  \       format: '%b:%l %t'
  \     }
  \   },
  \ })

autocmd User Ddu:ui:ff:openFilterWindow call s:ddu_filter_my_settings()
function! s:ddu_filter_my_settings() abort
  set cursorline

  call ddu#ui#ff#save_cmaps(['<C-j>', '<C-k>'])

  cnoremap <buffer> <CR>
    \ <Esc><Cmd>call ddu#ui#sync_action('itemAction')<CR>
  cnoremap <buffer> <Esc>
    \ <Esc><Cmd>call ddu#ui#sync_action('quit')<CR>
  cnoremap <buffer> <C-j>
    \ <Cmd>call ddu#ui#sync_action('cursorNext')<CR>
  cnoremap <buffer> <C-k>
    \ <Cmd>call ddu#ui#sync_action('cursorPrevious')<CR>
  cnoremap <buffer> <Down>
    \ <Cmd>call ddu#ui#sync_action('cursorNext')<CR>
  cnoremap <buffer> <Up>
    \ <Cmd>call ddu#ui#sync_action('cursorPrevious')<CR>
endfunction

autocmd User Ddu:ui:ff:closeFilterWindow call s:ddu_filter_cleanup()
function s:ddu_filter_cleanup() abort
  set nocursorline

  call ddu#ui#ff#restore_cmaps()
endfunction

" grep a fixed pattern and fuzzy-find on its search results
nnoremap <space>/
  \ :call ddu#start(#{
  \   sources: [#{name: 'rg', params: #{input: input('Pattern: ')}}],
  \ })<CR>
nnoremap <space>b :call ddu#start(#{sources: [#{name: 'buffer'}]})<CR>
nnoremap <space>f :call ddu#start(#{sources: [#{name: 'file_external'}]})<CR>
" f, but include hidden files (except .git/*)
nnoremap <space>h
  \ :call ddu#start(#{
  \   sources: [#{name: 'file_external', params: #{cmd: ['fd', '.', '-H', '-t', 'f', '-E', '.git']}}],
  \ })<CR>
nnoremap <space>m :call ddu#start(#{sources: [#{name: 'marks'}]})<CR>
nnoremap <space>l :call ddu#start(#{sources: [#{name: 'line'}]})<CR>
nnoremap <space>c :call ddu#start(#{sources: [#{name: 'command_history'}]})<CR>
nnoremap <space>r :call ddu#start(#{sources: [#{name: 'register'}]})<CR>
nnoremap <space>q :call ddu#start(#{sources: [#{name: 'qf'}]})<CR>

autocmd User Ddu:uiReady ++nested
  \ : if &l:filetype ==# 'ddu-ff'
  \ |   call ddu#ui#async_action('openFilterWindow')
  \ | endif
