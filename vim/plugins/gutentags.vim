if !has('nvim')
  if executable('rg')
    let g:gutentags_file_list_command = 'rg --files'
  endif
endif
