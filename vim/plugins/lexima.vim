let g:lexima_map_escape = '' " don't interfere with denite imap <Esc>

let s:lexima_completing_html = 0

function! CompleteHtml() abort
  if s:lexima_completing_html
    echom 'html tag completion disabled'
    call lexima#set_default_rules()
    let s:lexima_completing_html = 0
  else
    echom 'html tag completion enabled'
    call lexima#add_rule({'char': '<', 'input_after': '>'})
    call lexima#add_rule({'char': '>', 'at': '<\(\w\+\)\%#>', 'leave': 1, 'input_after': '</\1>', 'with_submatch': 1})
    let s:lexima_completing_html = 1
  endif
endfunction
