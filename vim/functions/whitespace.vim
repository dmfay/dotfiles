function! <SID>StripTrailingWhitespaces()
  if !&binary && &filetype != 'diff'
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
  endif
endfun

augroup cleanup_whitespace
  autocmd!
  autocmd BufWritePre *.{js,sql,java,c,h,cpp,rs,go,py,sh,zsh} :call <SID>StripTrailingWhitespaces()
  autocmd BufWritePre *.{json,xml,yml,yaml,plan,cfg,conf,ini,toml,md,rst,txt,example} :call <SID>StripTrailingWhitespaces()
  autocmd BufWritePre *.{html,css,scss,sass,less} :call <SID>StripTrailingWhitespaces()
  autocmd BufWritePre *.{vim,rc,service,timer,example} :call <SID>StripTrailingWhitespaces()
  autocmd BufWritePre Dockerfile :call <SID>StripTrailingWhitespaces()
augroup end
