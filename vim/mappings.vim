""""""""""
" mappings

" undo
nnoremap <F4> :MundoToggle<CR>

" and in insert mode!
inoremap <C-z> <C-g>u<C-w>
inoremap <C-u> <C-g>u<C-u>

" disable ex-mode
nnoremap Q q

" up/down in wrapped lines
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" screen up/down
nnoremap <C-j> <C-e>
nnoremap <C-k> <C-y>

" hide search highlighting
map <Leader>\ :noh<CR>

" duplicate a selection in visual mode
vmap D y'>p

" insert a single character
nnoremap <leader>I I <Esc>r
nnoremap <leader>i i <Esc>r
nnoremap <leader>A A <Esc>r
nnoremap <leader>a a <Esc>r

" next/previous lint symbols
nnoremap <space>n :lnext<CR>
nnoremap <space>p :lprev<CR>

""""""""""""""
" command line

cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-h> <Left>
cnoremap <C-l> <Right>
cnoremap <C-b> <C-Left>
cnoremap <C-f> <C-Right>

" cmdline paste
cnoremap <C-p> <C-r>*

""""""""
" splits

" new, absolute positions
nmap <leader>swh :topleft  vnew<CR>
nmap <leader>swl :botright vnew<CR>
nmap <leader>swk :topleft  new<CR>
nmap <leader>swj :botright new<CR>

" new, relative positions
nmap <leader>sh  :leftabove  vnew<CR>
nmap <leader>sl  :rightbelow vnew<CR>
nmap <leader>sk  :leftabove  new<CR>
nmap <leader>sj  :rightbelow new<CR>

" navigate splits
imap <C-w> <C-o><C-w>
nnoremap <A-h> <C-W>h
nnoremap <A-j> <C-W>j
nnoremap <A-k> <C-W>k
nnoremap <A-l> <C-W>l
nnoremap <A-Left> <C-W>h
nnoremap <A-Down> <C-W>j
nnoremap <A-Up> <C-W>k
nnoremap <A-Right> <C-W>l

""""""""""
" terminal

if has('nvim')
  tnoremap <C-[><C-[> <C-\><C-n>
  tnoremap <Esc><Esc> <C-\><C-n>
endif

"""""""""
" for osx

nnoremap ˙ <C-W>h
nnoremap ∆ <C-W>j
nnoremap ˚ <C-W>k
nnoremap ¬ <C-W>l

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" add undo points at punctuation, for prose
"
" intellij can't deal with it and doesn't expose a feature
" flag so nvim-only
"
" :h i_CTRL-G_u
" https://twitter.com/vimgifs/status/913390282242232320
if has('nvim')
  inoremap . .<C-g>u
  inoremap ? ?<C-g>u
  inoremap ! !<C-g>u
  inoremap , ,<C-g>u
endif
