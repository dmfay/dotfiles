""""""""""""
" statusline

set statusline=
set statusline+=%#Statement#
set statusline+=%(%m%h%r%w\ %)  " flags
set statusline+=%*
set statusline+=%f              " filename
set statusline+=%#FoldColumn#
set statusline+=%(\ \|\ %)
set statusline+=%*
set statusline+=%y              " filetype
set statusline+=%=
set statusline+=%l:%c\ \/\ %L   " line:col / maxline
set statusline+=%#FoldColumn#
set statusline+=%(\ \|\ %)
set statusline+=%*
set statusline+=%P              " % of file
