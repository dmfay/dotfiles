#!/usr/bin/env bash

set -e

services=''

os=`uname`

if [ "${os}" == 'Linux' ]; then
  os=`lsb_release -i -s | tr '[:upper:]' '[:lower:]'`
elif [ "${os}" == 'Darwin' ]; then
  os='osx'
fi

config="${os}.conf.yaml"

while getopts 'hsc:' flag; do
  case "$flag" in
    h)
      cat <<EOF
Set up system configuration.

Options:
  c
    Supply a different config file. Default is (os name lowercased).conf.yaml.
  s
    Additionally install systemctl services and timers.
EOF
      exit 0
      ;;
    s) services=true && shift ;;
    c) config="$OPTARG" && shift 2 ;;
  esac
done

dotbot_dir="dotbot"
dotbot_bin="bin/dotbot"
basedir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

cd "${basedir}"
git submodule update --init --recursive "${dotbot_dir}"

"${basedir}/${dotbot_dir}/${dotbot_bin}" -d "${basedir}" -c "${config}" "${@}"

if [[ ! -z "$services" ]]; then
  echo
  echo "Installing SystemD system services. This requires sudo."

  sudo cp systemd/system/* /usr/lib/systemd/system > /dev/null

  echo

  for f in systemd/system/*.timer; do
    timer=$(basename "$f")

    echo "Enabling $timer"

    sudo systemctl start "$timer" > /dev/null
    sudo systemctl enable "$timer" > /dev/null
  done

  echo "Reloading system daemons..."

  sudo systemctl daemon-reload > /dev/null

  echo "SystemD system services installed."
  exit 0
fi
