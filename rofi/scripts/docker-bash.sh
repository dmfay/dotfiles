#! /bin/bash

CONTAINER=$(docker ps | tail -n +2 | rofi -dmenu -p 'select container' | cut -d ' ' -f 1)

if [[ -z "$CONTAINER" ]]; then
  echo 'Cancel'
else
  rofi-sensible-terminal start -- docker exec -ti "$CONTAINER" /bin/bash
fi
