#!/bin/sh

pac=$(checkupdates 2>/dev/null | wc -l)
aur=$(pikaur -Qua 2>/dev/null | wc -l)

check=$((pac + aur))
if [[ "$check" != "0" ]]
then
    echo "$pac/$aur"
else
    echo ""
fi
