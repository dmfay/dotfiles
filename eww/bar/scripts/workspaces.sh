#!/bin/sh

workspaces() {
  # OKAY:
  #
  # (box
  #   :halign \"center\"
  #   :valign \"center\"
  #   :vexpand true
  #   :hexpand true
  #   :class \"workspaces\"
  #   :orientation \"h\"
  #   :spacing 0
  #   :space-evenly \"false\"
  #
  # so far, so straightforward. now, get all workspaces and parse the json
  # swaymsg hands back into individual buttons.
  #
  #   $(
  #     swaymsg -t get_workspaces |
  #
  # -r for raw, otherwise each eventbox-string will be quoted. backslashed open
  # paren denotes interpolation into the jq string, and initially we're dumping
  # each element of the array into a filter producing an eventbox.
  #
  #     jq -r '"\(.[] | "
  #       (eventbox
  #         :cursor \"pointer\"
  #         (button
  #
  # interpolate .name into the change-workspace swaymsg
  #
  #           :onclick \"swaymsg workspace number \(.name)\"
  #
  # add the relevant class when the ws is focused or urgent
  #
  #           \(if .focused then ":class \"focused\"" elif .urgent then ":class \"urgent\"" else "" end)
  #
  # finally print the name on the button
  #
  #           \(.name)"
  #         )
  #       )
  #     )"'
  #   )
  # )


  echo "(box :halign \"center\" :valign \"center\" :vexpand true :hexpand true :class \"workspaces\" :orientation \"h\" :spacing 0 :space-evenly \"false\" $(swaymsg -t get_workspaces | jq -r '"\(.[] | "(eventbox :cursor \"pointer\" (button :onclick \"swaymsg workspace number \(.name)\" \(if .focused then ":class \"focused\"" elif .urgent then ":class \"urgent\"" else "" end) \(.name)") ))"' | tr '\n' ' '))"
}

workspaces

swaymsg -t subscribe -m '["workspace"]' | while read -r _ ; do
  workspaces
done
