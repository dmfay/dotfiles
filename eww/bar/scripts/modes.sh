#!/bin/sh

echo ""

swaymsg -t subscribe -m '["mode"]' | while read -r mode ; do
  echo "$mode" | jq -r '.change | if . == "default" then "" else . end'
done
