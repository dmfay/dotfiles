#! /bin/sh

networks() {
  if [[ "$2" == "up" ]]; then
    next_up=`cat /sys/class/net/$1/statistics/tx_bytes`
    let "current=next_up - $3"
  else
    next_dn=`cat /sys/class/net/$1/statistics/rx_bytes`
    let "current=next_dn - $3"
  fi

  echo "$current"
}

networks "$1" "$2" "$3"
