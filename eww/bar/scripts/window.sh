#!/bin/sh

swaymsg -t subscribe -m '["window", "workspace"]' | while read -r _; do
  swaymsg -t get_tree | jq -r '[recurse(.nodes[])] | map(select(.focused and .type == "con")) | if length > 0 then .[].name else "" end'
done
