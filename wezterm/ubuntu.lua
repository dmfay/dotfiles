local wezterm = require 'wezterm';

return {
  check_for_updates = false,
  scrollback_lines = 50000,
  term = "xterm-256color",
  enable_tab_bar = false,
  default_cursor_style = "BlinkingBlock",

  window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
  },

  font = wezterm.font_with_fallback({
    "Inconsolata LGC",    -- Latin/Greek/Cyrillic with bold+italic variants
    "DejaVu Sans Mono",   -- ditto
    "FontAwesome",        -- ditto
    "Symbola",            -- ditto
    "JoyPixels"           -- emoji
  }),
  font_size = 11.8,
  line_height = 0.9,

  freetype_interpreter_version = 40,
  dpi = 96.0,
  colors = {
    background = "#3c3445",
    foreground = "#f5eff3",

    cursor_border = "#f5eff3",
    cursor_bg = "#f5eff3",
    cursor_fg = "#3c3445",

    selection_bg = "#604c60",
    selection_fg = "#f5eff3",

    ansi = {
      "#3c3445",
      "#9c6b80",
      "#6e916e",
      "#9c8969",
      "#6582c7",
      "#a276b6",
      "#6698aa",
      "#cfc7cd"
    },
    brights = {
      "#604c60",
      "#dc98ac",
      "#96c497",
      "#d9bd8f",
      "#98b2fb",
      "#d6ade8",
      "#9ccbdd",
      "#f5eff3"
    }
  }
}
